my-qa-framework
===================

## Pre-requisites

1. Maven is installed and the environment variable set (From the command line run "mvn -v")
2. Java is installed and the environment variable set (From the command line run "java -version")
3. Git is installed and the environment variable set (From the command line run "git --version")


This framework provides the capability to create and execute business facing tests using the BDD software development process.The key features provided are:

1. Tests can be written in feature files using the "Given, When, Then" structure (by using Gherkin, which is the name of Cucumber's DSL).


> ***Platform and Browser compatibility testing***
> This framework has been written in such away that test scripts can be executed on firefox and chrome browsers within following operating systems..
  - MAC OS
  - Linux
  - Windows
  

### < env >.properties
If you have environment specific configuration that your tests need access to at runtime, then an environment file can be created. For example I have created few sample properties files. You need to pass environment variable to "environment" variable if you want to test on specific env. 
By default it takes env.properties

Usage
-----

The framework is divided as follows:

| Folder             | Description                           |
| ------------------ | --------------------------------------|
| src/main/java      | Constants, Page Objects and utilities |
| src/main/resources | Property files                        |
| src/test/java      | Step definitions and Runner           |
| src/test/resources | Test resources and Feature files      |

The tests can be executed in a number of ways in Maven:

| Command																| Description                                           |
| --------------------------------------------------------------------- | ------------------------------------------------------|
| `mvn clean test`													    | Run all features                                      |
| `mvn clean test -Dbrowser=firefox`								    | Run all features on firefox browser                   |
| `mvn clean test -Dbrowser=chrome`									    | Run all features on chrome browser                    |
| `mvn clean test -Dcucumber.options="--tags <tag name>"`			    | Run specific Feature(s)/Scenario(s) which are tagged  | 

Reports
-------
Once you have executed the tests, framework will create reports under the `/target/cucumber/index.html`. Here you can view the test execution status.

