
Feature: Hotel Booking System
  As I customer I need Online Hotel Booking System
  So That I can book,delete and manage my hotel bookings

  @createHotelBooking @data-cleanup
  Scenario: Create a hotel booking
    Given I navigate to Equal Experts Hotel Booking site
    When I enter booking details Firstname "Venkat", Lastname "Kolli", Price "1000", with Deposit, Checkin "2018-05-25" and Checkout "2018-05-28"
    And I click Save button
    Then booking should be successful with details Firstname "Venkat", Lastname "Kolli", Price "1000", Deposit "true", Checkin "2018-05-25" and Checkout "2018-05-28"

  @createMultipleBooking
  Scenario: Create multiple hotel booking
    Given I navigate to Equal Experts Hotel Booking site
    Then I create booking by entering following details
      | Firstname | Lastname | Price | Checkin    | Checkout   |
      | Suresh    | Kolli    | 1000  | 2018-05-25 | 2018-05-28 |
      | Satya     | Kolli    | 1000  | 2018-05-25 | 2018-05-28 |
    Then booking should be successful with details
      | Firstname | Lastname | Price | Checkin    | Checkout   |
      | Suresh    | Kolli    | 1000  | 2018-05-25 | 2018-05-28 |
      | Satya     | Kolli    | 1000  | 2018-05-25 | 2018-05-28 |

  @deleteHotelBooking
  Scenario: Cancel a hotel booking-Suresh
    Given I navigate to Equal Experts Hotel Booking site
    When I cancel booking for details Firstname "Suresh"
    Then booking should be deleted for FirstName "Suresh"

  @deleteHotelBooking
  Scenario: Cancel a hotel booking-Satya
    Given I navigate to Equal Experts Hotel Booking site
    When I cancel booking for details Firstname "Satya"
    Then booking should be deleted for FirstName "Staya"

  @deleteHotelBooking
  Scenario: Cancel a hotel booking-Venkat
    Given I navigate to Equal Experts Hotel Booking site
    When I cancel booking for details Firstname "Venkat"
    Then booking should be deleted for FirstName "Venkat"

