package com.swades.hooks;

import com.swades.page_objects.HotelBookingPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.io.IOException;

public class Hooks {

    private HotelBookingPage hotelBookingPage ;

    public Hooks() throws IOException {
        hotelBookingPage = new HotelBookingPage();
    }

    @Before("@data-cleanup")
    public void testSetUp(){
        hotelBookingPage.navigateToHotelBookingPage();
        hotelBookingPage.deleteAllRecords("firstName");
    }

    @After
    public void tearDown() {
        hotelBookingPage.tearDown();
    }

}
