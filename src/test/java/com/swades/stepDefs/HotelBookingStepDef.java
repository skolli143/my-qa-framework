package com.swades.stepDefs;

import com.swades.hooks.Hooks;
import com.swades.page_objects.HotelBookingPage;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.BeforeClass;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class HotelBookingStepDef {

    private HotelBookingPage hotelBookingPage;

    public HotelBookingStepDef() throws IOException {
        hotelBookingPage = new HotelBookingPage();
    }

    @Before("@data-cleanup")
    public void dataCleanUp(){
        hotelBookingPage.navigateToHotelBookingPage();
        hotelBookingPage.deleteAllRecords("firstName");
    }

    @Given("^I navigate to Equal Experts Hotel Booking site$")
    public void i_navigate_to_Equal_Experts_Hotel_Booking_site() {
        hotelBookingPage.navigateToHotelBookingPage();
        assertEquals("Hotel booking form", hotelBookingPage.getHotelBookingPageHeading());
    }

    @Then("^I create booking by entering following details$")
    public void i_enter_below_booking_details(DataTable input) {

        List<Map<String, String>> values = input.asMaps(String.class, String.class);

        for (Map<String, String> row : values) {
            hotelBookingPage.enterFirstName(row.get("Firstname").toString());
            hotelBookingPage.enterLastName(row.get("Lastname").toString());
            hotelBookingPage.enterPrice(row.get("Price").toString());
            hotelBookingPage.selectDepositOption();
            hotelBookingPage.enterCheckin(row.get("Checkin").toString());
            hotelBookingPage.enterCheckout(row.get("Checkout").toString());
            hotelBookingPage.clickSaveButton();
        }
    }

    @Then("^booking should be successful with details$")
    public void booking_should_be_successful_with_following_details(DataTable input) {

        List<Map<String, String>> values = input.asMaps(String.class, String.class);

        for (Map<String, String> row : values) {

            assertEquals(row.get("Firstname").toString(), hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("firstName", "firstName", row.get("Firstname").toString()));
            assertEquals(row.get("Lastname").toString(), hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("lastName", "firstName", row.get("Firstname").toString()));
            assertEquals(row.get("Price").toString(), hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("price", "firstName", row.get("Firstname").toString()));
            assertEquals(row.get("Checkin").toString(), hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("checkin", "firstName", row.get("Firstname").toString()));
            assertEquals(row.get("Checkout").toString(), hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("checkout", "firstName", row.get("Firstname").toString()));

        }
    }

    @When("^I enter booking details Firstname \"([^\"]*)\", Lastname \"([^\"]*)\", Price \"([^\"]*)\", with Deposit, Checkin \"([^\"]*)\" and Checkout \"([^\"]*)\"$")
    public void i_enter_booking_details(String firstName, String lastName, String price, String checkin, String checkout) throws Exception {
        hotelBookingPage.enterFirstName(firstName);
        hotelBookingPage.enterLastName(lastName);
        hotelBookingPage.enterPrice(price);
        hotelBookingPage.selectDepositOption();
        hotelBookingPage.enterCheckin(checkin);
        hotelBookingPage.enterCheckout(checkout);
    }

    @When("^I click Save button$")
    public void i_click_Save_button() throws Exception {
        hotelBookingPage.clickSaveButton();
    }

    @Then("^booking should be successful with details Firstname \"([^\"]*)\", Lastname \"([^\"]*)\", Price \"([^\"]*)\", Deposit \"([^\"]*)\", Checkin \"([^\"]*)\" and Checkout \"([^\"]*)\"$")
    public void booking_should_be_successful(String firstName, String lastName, String price, String depositOption, String checkin, String checkout) {
        assertEquals(firstName, hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("firstName", "firstName", firstName));
        assertEquals(lastName, hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("lastName", "firstName", firstName));
        assertEquals(price, hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("price", "firstName", firstName));
        assertEquals(checkin, hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("checkin", "firstName", firstName));
        assertEquals(checkout, hotelBookingPage.returnValueForGivenColumnNameAndForKeyValue("checkout", "firstName", firstName));
    }

    @When("^I cancel booking for details Firstname \"([^\"]*)\"$")
    public void i_cancel_booking_for_details_Firstname(String firstNameValue) {
        hotelBookingPage.deleteRecordForGivenColumnName("firstName", firstNameValue);
    }

    @Then("^booking should be deleted for FirstName \"([^\"]*)\"$")
    public void booking_should_be_deleted_for_FirstName(String firstName) throws InterruptedException {
        assertFalse(hotelBookingPage.checkRecordDeleted("firstName", firstName));
    }

    @After
    public void tearDown() {
        hotelBookingPage.tearDown();
    }

}
