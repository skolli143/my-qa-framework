package com.swades.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

    @RunWith(Cucumber.class)
    @CucumberOptions(
            format = {"json:target/cucumber.json", "html:target/cucumber" },
            features = "src/test/resources/features" ,
            glue = {"com.swades.stepDefs","com.swades.hooks"},
            tags = {}
    )

    public class RunCukesTest {
    }

