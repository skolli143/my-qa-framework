package com.swades.backEndTests;

import org.junit.Test;

import java.io.*;

public class FileTests {


    @Test
    public void xmlToString() throws IOException {

        // filename is filepath string
        BufferedReader br = new BufferedReader(new FileReader(new File("src/test/resources/test-files/testXml.xml")));
        String line;
        StringBuffer sb = new StringBuffer();

        while ((line=br.readLine())!=null){
            sb.append(line.trim());
        }

        System.out.println("Converted String : "+sb.toString());

    }
}
