package com.swades.backEndTests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swades.pojos.*;
import io.restassured.response.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RestAPI {

    //Setup
    // npm install -g json-server
    // json-server --watch db.json

    @Test
    public void getTest() throws IOException {
        Response response =  given().
                when()
                .header("Content-Type","application/json")
                .get("http://localhost:3000/posts/1")
                .then().extract().response();

        System.out.println("------------"+response.getStatusCode());
        System.out.println("------------"+response.getBody().asString());


        ObjectMapper mapper = new ObjectMapper();
        Result result = mapper.readValue(response.getBody().asString(),Result.class);

        System.out.println("-----Author-------"+result.getAuthor());

    }

    @Test
    public void firstPostTest(){
       Response response =  given().
                when()
                .header("Content-Type","application/json")
                .body("{\n" +
                        "    \"id\": 25,\n" +
                        "    \"title\": \"post test\",\n" +
                        "    \"author\": \"sureshKolli25\"\n" +
                        "  }")
                .post("http://localhost:3000/posts")
                .then().extract().response();

        System.out.println("------------"+response.getStatusCode());

    }

    @Test
    public void secondPutTest(){
        Response response =  given().
                when()
                .header("Content-Type","application/json")
                .body("{\n" +
                        "    \"id\": 25,\n" +
                        "    \"title\": \"put test\",\n" +
                        "    \"author\": \"sureshKolli25\"\n" +
                        "  }")
                .put("http://localhost:3000/posts/25")
                .then().extract().response();

        System.out.println("------------"+response.getStatusCode());

    }

    @Test
    public void thirdDeleteTest(){
        Response response =  given().
                when()
                .header("Content-Type","application/json")
                .delete("http://localhost:3000/posts/25")
                .then().extract().response();

        System.out.println("------------"+response.getStatusCode());

    }

    @Test
    @Ignore
    @JsonIgnoreProperties(ignoreUnknown = true)
    public void parsingJson() throws IOException{

        ObjectMapper mapper = new ObjectMapper();

        InputJson inputJson = mapper.readValue(new File("src/test/resources/test-files/InputJson.json"),InputJson.class);

        Vehicles[] vehiclesList = inputJson.getVehicles();
        for (Vehicles vehicles : vehiclesList) {
            Vehicle[] vehicleList =vehicles.getVehicle();
            for (Vehicle vehicle : vehicleList) {
                System.out.println("Vehicle ID :"+vehicle.getVId());
                System.out.println("Vehicle ID :"+vehicle.getVName());

                Department[] departmentList = vehicle.getDepartment();
                for (Department department : departmentList) {
                    System.out.println("Department Name :"+department.getDepartmentName());
                    System.out.println("Department ID :"+department.getDepId());

                    Item[] itemList = department.getItem();
                    for (Item item : itemList) {
                        System.out.println("Item ID :"+item.getItemId());
                        System.out.println("Item Name :"+item.getItemName());
                        System.out.println("------------------------------");
                    }
                }
            }
        }
        }
}
