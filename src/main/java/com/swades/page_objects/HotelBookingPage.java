package com.swades.page_objects;

import com.swades.constants.HotelBookingPageConstants;
import com.swades.config.BrowserSelector;
import com.swades.config.TestProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class HotelBookingPage {

    public static final Logger LOGGER = LoggerFactory.getLogger(HotelBookingPage.class);

    private WebDriver driver;
    private TestProperties testProperties;

    public HotelBookingPage() throws IOException {
        BrowserSelector browserSelector = new BrowserSelector();
        testProperties = new TestProperties();
        this.driver = browserSelector.getDriver();
    }

    private final String hotelBookingPageHeading = "html/body/div[1]/div[1]/h1";

    private final String txtFirstName = "//*[@id='firstname']";
    private final String txtLastName = "//*[@id='lastname']";
    private final String txtPrice = "//*[@id='totalprice']";
    private final String drbDeposit = "//*[@id='depositpaid']";
    private final String txtCheckin = "//*[@id='checkin']";
    private final String txtCheckout = "//*[@id='checkout']";
    private final String btnSave = "//input[contains(@value,'Save')]";


    public void navigateToHotelBookingPage() {
        driver.get(testProperties.getProperty("hotelBookingSiteURL"));
    }

    public String getHotelBookingPageHeading() {
        return driver.findElement(By.xpath(hotelBookingPageHeading)).getText();
    }

    public void enterFirstName(String firstName) {
        sendChar(driver.findElement(By.xpath(txtFirstName)), firstName);
    }

    public void enterLastName(String lastName) {
        sendChar(driver.findElement(By.xpath(txtLastName)), lastName);
    }

    public void enterPrice(String price) {
        sendChar(driver.findElement(By.xpath(txtPrice)), price);
    }

    public void selectDepositOption() {
        Select depositOption = new Select(driver.findElement(By.xpath(drbDeposit)));
        depositOption.selectByIndex(1);
    }

    public void enterCheckin(String checkin) {
        sendChar(driver.findElement(By.xpath(txtCheckin)), checkin);
    }

    public void enterCheckout(String checkout) {
        sendChar(driver.findElement(By.xpath(txtCheckout)), checkout);
    }

    public void clickSaveButton() {
        driver.findElement(By.xpath(btnSave)).click();
    }

    private String returnColumnNumberForGivenColumnName(String colName) {

        String colNum = null;

        switch (colName) {
            case "firstName":
                colNum = HotelBookingPageConstants.FIRST_NAME_COLUMN_NUMBER;
                break;
            case "lastName":
                colNum = HotelBookingPageConstants.LAST_NAME_COLUMN_NUMBER;
                break;
            case "price":
                colNum = HotelBookingPageConstants.PRICE_COLUMN_NUMBER;
                break;
            case "checkin":
                colNum = HotelBookingPageConstants.CHECK_IN_COLUMN_NUMBER;
                break;
            case "checkout":
                colNum = HotelBookingPageConstants.CHECK_OUT_COLUMN_NUMBER;
                break;
            default:
                LOGGER.error("not a valid columnName");
                break;
        }
        return colNum;
    }

    private String returnRowNumberForGivenColumnNameAndValue(String colName, String sValue) {

        int rowNum = 2;
        String colNum = returnColumnNumberForGivenColumnName(colName);

        List<WebElement> cellData =
                driver.findElements(By.xpath("//*[@id='bookings']/div[contains(@class,'row')]/div[" + colNum + "]/p"));
        for (WebElement row : cellData) {
            if (row.getText().equalsIgnoreCase(sValue)) {
                break;
            }
            rowNum++;
        }
        return Integer.toString(rowNum);
    }

    public void deleteAllRecords(String colName) {

        String colNum = returnColumnNumberForGivenColumnName(colName);

        List<WebElement> cellData =
                driver.findElements(By.xpath("//*[@id='bookings']/div[contains(@class,'row')]/div[" + colNum + "]/p"));
        int rowNum;
        if(cellData.size()!=0) {
            for (int i = 0; i < cellData.size(); i++) {
                rowNum = i + 2;
                String elementXpath = "//*[@id='bookings']/div[" + rowNum + "]/div[7]/input";
                WebElement element = driver.findElement(By.xpath(elementXpath));
                element.click();
            }
            String elementXpath = "//*[@id='bookings']/div[2]/div[7]/input";
            WebElement element = driver.findElement(By.xpath(elementXpath));
            element.click();
        }

    }

    public String returnValueForGivenColumnNameAndForKeyValue(String colName, String keyColName, String keyValue) {

        String colNum = returnColumnNumberForGivenColumnName(colName);
        String rowNum = returnRowNumberForGivenColumnNameAndValue(keyColName, keyValue);

        String elementXpath = "//*[@id='bookings']/div[" + rowNum + "]/div[" + colNum + "]/p";
        WebElement element = driver.findElement(By.xpath(elementXpath));

        return element.getText();
    }

    public Boolean checkRecordDeleted(String colName, String sValue) throws InterruptedException {
        driver.navigate().refresh();
        boolean result = false;

        String colNum = returnColumnNumberForGivenColumnName(colName);

        List<WebElement> cellData =
                driver.findElements(By.xpath("//*[@id='bookings']/div[contains(@class,'row')]/div[" + colNum + "]/p"));

        for (int i = 0; i < cellData.size() - 1; i++) {
            if (cellData.get(i).getText().equalsIgnoreCase(sValue)) {
                result = true;
                break;
            }
        }
        return result;
    }

    public void deleteRecordForGivenColumnName(String keyCloName, String sValue) {

        String rowNum = returnRowNumberForGivenColumnNameAndValue(keyCloName, sValue);

        String elementXpath = "//*[@id='bookings']/div[" + rowNum + "]/div[7]/input";
        WebElement element = driver.findElement(By.xpath(elementXpath));

        element.click();
    }

    public void sendChar(WebElement element, String value) {
        element.clear();

        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            String s = new StringBuilder().append(c).toString();
            element.sendKeys(s);
        }
    }

    public void tearDown() {
        this.driver.quit();
    }
}
