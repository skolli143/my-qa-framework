package com.swades.constants;

public class HotelBookingPageConstants {

    public static final String FIRST_NAME_COLUMN_NUMBER ="1";
    public static final String LAST_NAME_COLUMN_NUMBER ="2";
    public static final String PRICE_COLUMN_NUMBER ="3";
    public static final String CHECK_IN_COLUMN_NUMBER ="5";
    public static final String CHECK_OUT_COLUMN_NUMBER ="6";

}
