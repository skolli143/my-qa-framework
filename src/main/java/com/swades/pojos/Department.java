package com.swades.pojos;

public class Department {

    private Item[] item;

    private String departmentName;

    private String depId;

    public Item[] getItem ()
    {
        return item;
    }

    public void setItem (Item[] Item)
    {
        this.item = item;
    }

    public String getDepartmentName ()
    {
        return departmentName;
    }

    public void setDepartmentName (String departmentName)
    {
        this.departmentName = departmentName;
    }

    public String getDepId ()
    {
        return depId;
    }

    public void setDepId (String depId)
    {
        this.depId = depId;
    }

}
