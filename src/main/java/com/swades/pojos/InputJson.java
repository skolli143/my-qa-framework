package com.swades.pojos;

public class InputJson {

    private Vehicles[] vehicles;

    public Vehicles[] getVehicles ()
    {
        return vehicles;
    }

    public void setVehicles (Vehicles[] vehicles)
    {
        this.vehicles = vehicles;
    }

}
