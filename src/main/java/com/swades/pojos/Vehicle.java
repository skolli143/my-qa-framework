package com.swades.pojos;

public class Vehicle {
    private String VName;

    private Department[] department;

    private int vId;

    public String getVName ()
    {
        return VName;
    }

    public void setVName (String VName)
    {
        this.VName = VName;
    }

    public Department[] getDepartment ()
    {
        return department;
    }

    public void setDepartment (Department[] department)
    {
        this.department = department;
    }

    public int getVId ()
    {
        return vId;
    }

    public void setVId (int vId)
    {
        this.vId = vId;
    }
}
